require 'spec_helper'

describe ParserBu do
  let(:file_bu_name){ Rails.root.to_s + '/spec/files/bweb_1t_RS_10102012014547.part2.csv'}

  it "should parser file using block" do
    parser = ParserBu.new(file_bu_name)
    parser.parse_file_to_boletim_urna
    BoletimUrna.count.should == 3365
  end
end
