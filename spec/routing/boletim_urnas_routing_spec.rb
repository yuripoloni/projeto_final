require "spec_helper"

describe BoletimUrnasController do
  describe "routing" do

    it "routes to #index" do
      get("/boletim_urnas").should route_to("boletim_urnas#index")
    end

    it "routes to #new" do
      get("/boletim_urnas/new").should route_to("boletim_urnas#new")
    end

    it "routes to #show" do
      get("/boletim_urnas/1").should route_to("boletim_urnas#show", :id => "1")
    end

    it "routes to #edit" do
      get("/boletim_urnas/1/edit").should route_to("boletim_urnas#edit", :id => "1")
    end

    it "routes to #create" do
      post("/boletim_urnas").should route_to("boletim_urnas#create")
    end

    it "routes to #update" do
      put("/boletim_urnas/1").should route_to("boletim_urnas#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/boletim_urnas/1").should route_to("boletim_urnas#destroy", :id => "1")
    end

  end
end
