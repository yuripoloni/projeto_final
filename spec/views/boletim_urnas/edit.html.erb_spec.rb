require 'spec_helper'

describe "boletim_urnas/edit" do
  before(:each) do
    @boletim_urna = assign(:boletim_urna, stub_model(BoletimUrna))
  end

  it "renders the edit boletim_urna form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", boletim_urna_path(@boletim_urna), "post" do
    end
  end
end
