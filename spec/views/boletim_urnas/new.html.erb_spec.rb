require 'spec_helper'

describe "boletim_urnas/new" do
  before(:each) do
    assign(:boletim_urna, stub_model(BoletimUrna).as_new_record)
  end

  it "renders new boletim_urna form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", boletim_urnas_path, "post" do
    end
  end
end
