# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140826010014) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bulletin_urns", force: true do |t|
    t.date    "data_da_geracao"
    t.time    "hora_da_geracao"
    t.integer "codigo_pleito"
    t.integer "codigo_eleicao"
    t.date    "data_recebimento_bu"
    t.integer "quantidade_eleitores_aptos"
    t.integer "quantidade_eleitores_faltosos"
    t.integer "quantidade_comparecimento"
    t.integer "codigo_tipo_eleicao"
    t.string  "nome_da_origem_de_votos"
    t.integer "codigo_tipo_urna"
    t.string  "descricao_urna"
    t.string  "numero_de_urna_efetivada"
    t.string  "codigo_da_carga_urna_1"
    t.string  "codigo_da_carga_urna_2"
    t.date    "data_da_carga_de_urna"
    t.string  "codigo_do_flashcard_da_urna"
    t.string  "cargo_pergunta_secao"
  end

  create_table "bulletin_urns_candidates_electoral_sections_political_parties", force: true do |t|
    t.integer "bulletin_urns_id"
    t.integer "political_office_id"
    t.integer "election_number"
    t.integer "vote_amount"
    t.integer "candidate_id"
    t.integer "electoral_section_id"
    t.integer "political_party_id"
  end

  add_index "bulletin_urns_candidates_electoral_sections_political_parties", ["election_number", "candidate_id", "electoral_section_id", "political_party_id", "political_office_id"], name: "unique_bulletin_urns", unique: true, using: :btree
  add_index "bulletin_urns_candidates_electoral_sections_political_parties", ["political_office_id"], name: "unique_bulletin_urns_pollitical_office", using: :btree

  create_table "candidates", force: true do |t|
    t.string  "name"
    t.integer "code"
  end

  create_table "elections_electoral_sections", force: true do |t|
    t.integer  "election_number"
    t.integer  "electoral_section_id"
    t.integer  "number_of_voters"
    t.integer  "number_of_voters_not_appeared"
    t.integer  "number_of_voters_appeared"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "elections_electoral_sections", ["election_number", "electoral_section_id"], name: "unique_electoral_sections", unique: true, using: :btree
  add_index "elections_electoral_sections", ["electoral_section_id"], name: "index_elections_electoral_sections_on_electoral_section_id", using: :btree

  create_table "electoral_sections", force: true do |t|
    t.string  "city"
    t.boolean "capital"
    t.string  "district"
    t.string  "street"
    t.string  "zip_code"
    t.string  "state"
    t.string  "zone"
    t.string  "section"
    t.boolean "active"
    t.string  "polling_place"
    t.float   "latitude"
    t.float   "longitude"
  end

  add_index "electoral_sections", ["state", "zone"], name: "index_electoral_sections_on_state_and_zone", using: :btree

  create_table "political_offices", force: true do |t|
    t.integer "code"
    t.string  "name"
  end

  create_table "political_parties", force: true do |t|
    t.string  "name"
    t.integer "code"
  end

end
