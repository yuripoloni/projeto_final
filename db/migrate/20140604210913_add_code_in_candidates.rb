class AddCodeInCandidates < ActiveRecord::Migration
  def change
    change_table(:cadidates) do |t|
      t.column :code, :integer
    end
    rename_table :cadidates, :candidates
  end
end
