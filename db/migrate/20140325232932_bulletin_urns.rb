class BulletinUrns < ActiveRecord::Migration
  def change
    create_table(:bulletin_urns) do |t|
      t.column :data_da_geracao, :date
      t.column :hora_da_geracao, :time
      t.column :codigo_pleito, :integer
      t.column :codigo_eleicao, :integer
      # t.column :sigla_uf, :string, size: 2
      # t.column :codigo_cargo, :integer
      # t.column :descricao_cargo, :string
      # t.column :numero_zona, :integer
      # t.column :numero_secao, :integer
      # t.column :numero_local_votacao, :integer
      # t.column :numero_partido, :integer
      # t.column :numero_partido, :integer
      # t.column :nome_partido, :string
      # t.column :codigo_municipio, :integer
      # t.column :nome_municipio, :string
      t.column :data_recebimento_bu, :date
      t.column :quantidade_eleitores_aptos, :integer
      t.column :quantidade_eleitores_faltosos, :integer
      t.column :quantidade_comparecimento, :integer
      t.column :codigo_tipo_eleicao, :integer
      t.column :nome_da_origem_de_votos, :string
      t.column :codigo_tipo_urna, :integer
      t.column :descricao_urna, :string
      # t.column :numero_do_votavel, :integer
      # t.column :nome_do_votavel, :string
      # t.column :quantidade_votos, :integer
      # t.column :codigo_do_tipo_do_votavel, :integer
      t.column :numero_de_urna_efetivada, :string
      t.column :codigo_da_carga_urna_1, :string
      t.column :codigo_da_carga_urna_2, :string
      t.column :data_da_carga_de_urna, :date
      t.column :codigo_do_flashcard_da_urna, :string
      t.column :cargo_pergunta_secao, :string
    end
  end
end


