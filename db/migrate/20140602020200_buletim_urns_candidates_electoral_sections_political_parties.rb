class BuletimUrnsCandidatesElectoralSectionsPoliticalParties < ActiveRecord::Migration
  def change
    create_table(:bulletin_urns_candidates_electoral_sections_political_parties) do |t|
      t.belongs_to :bulletin_urns
      t.belongs_to :candidates
      t.belongs_to :electoral_sections
      t.belongs_to :political_parties

      t.column :quantidade_votos, :integer
    end
  end
end
