class ChangeBulletinUrnsCandidatesElectoralSectionsPoliticalParties < ActiveRecord::Migration
  def change
    change_table(:bulletin_urns_candidates_electoral_sections_political_parties) do |t|
      t.column :election_number, :integer
      t.column :vote_amount, :integer

      t.remove :candidates_id
      t.belongs_to :candidate
      t.remove :electoral_sections_id
      t.belongs_to :electoral_section
      t.remove :political_parties_id
      t.belongs_to :political_party

      t.remove :quantidade_votos

    end
  end
end
