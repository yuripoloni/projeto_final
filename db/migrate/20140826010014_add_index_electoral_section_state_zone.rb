class AddIndexElectoralSectionStateZone < ActiveRecord::Migration
  def change
    add_index(:electoral_sections, [:state,:zone])
  end
end
