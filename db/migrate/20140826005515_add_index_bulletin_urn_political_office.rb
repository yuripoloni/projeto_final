class AddIndexBulletinUrnPoliticalOffice < ActiveRecord::Migration
  def change
    add_index(:bulletin_urns_candidates_electoral_sections_political_parties,
     [:political_office_id], name: "unique_bulletin_urns_pollitical_office")
  end
end
