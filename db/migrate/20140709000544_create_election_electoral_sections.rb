class CreateElectionElectoralSections < ActiveRecord::Migration
  def change
    create_table :elections_electoral_sections do |t|
      t.integer :election_number, index: true
      t.belongs_to :electoral_section, index: true
      t.integer :number_of_voters
      t.integer :number_of_voters_not_appeared
      t.integer :number_of_voters_appeared

      t.timestamps
    end

    add_index(:elections_electoral_sections,
     [:election_number, :electoral_section_id], unique: true, name: "unique_electoral_sections")
  end
end
