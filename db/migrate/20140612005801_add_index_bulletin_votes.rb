class AddIndexBulletinVotes < ActiveRecord::Migration
  def change
    add_index(:bulletin_urns_candidates_electoral_sections_political_parties,
     [:election_number, :candidate_id, :electoral_section_id,
      :political_party_id, :political_office_id], unique: true, name: "unique_bulletin_urns")
  end
end
