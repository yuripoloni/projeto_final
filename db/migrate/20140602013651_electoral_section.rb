class ElectoralSection < ActiveRecord::Migration
  def change
    create_table(:electoral_sections) do |t|
      t.column :city, :string
      t.column :capital, :boolean
      t.column :district, :string
      t.column :street, :string
      t.column :zip_code, :string
      t.column :state, :string, size: 2
      t.column :zone, :string
      t.column :section, :string
      t.column :active, :boolean
      t.column :polling_place, :string
      t.column :latitude, :float
      t.column :longitude, :float
    end
  end
end
