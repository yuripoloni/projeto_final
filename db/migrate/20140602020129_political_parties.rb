class PoliticalParties < ActiveRecord::Migration
  def change
    create_table(:political_parties) do |t|
      t.column :name, :string
      t.column :code, :integer
    end
  end
end
