class CreatePoliticalOffice < ActiveRecord::Migration
  def change
    create_table :political_offices do |t|
      t.column :code, :integer
      t.column :name, :string
    end
    change_table(:bulletin_urns_candidates_electoral_sections_political_parties) do |t|
      t.belongs_to :political_office
    end
  end
end
