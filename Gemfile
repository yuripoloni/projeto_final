source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.4'

# Use sqlite3 as the database for Active Record
gem 'pg'

# Use SCSS for stylesheets
gem 'bootstrap-sass', '~> 3.2.0'
gem 'sass-rails', '>= 3.2'
# gem 'autoprefixer-rails'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :development, :test do
  gem 'pry'
  gem 'rspec'
  gem 'rspec-rails'
  gem 'grb'
  gem 'capistrano'
  gem 'capistrano-rvm'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
  gem 'capistrano-thin'
end

# Use to generate graphic
gem "chartkick"
gem "rabl"

# Use to geographic localization. Read more: https://github.com/alexreisner/geocoder
gem "geocoder"

# Use to graph database client
#gem "neo4j"
#gem 'neo4j-core'
#gem 'neo4j', github: 'andreasronge/neo4j'
# Use to parser HTML
gem 'nokogiri'
gem 'httpi'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

gem "progress_bar"

# Use unicorn as the app server
# gem 'unicorn'

gem "sidekiq"
gem 'sinatra'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

gem 'thin'

# DataMining
gem 'dbscan'

gem 'ransack'

gem 'select2-rails'
