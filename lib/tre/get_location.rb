module Tre
  class GetLocation
    attr_accessor :zona_id

    @@default_url = 'http://www.tre-rs.gov.br/apps/locais/index.php'

    def initialize(zona_id)
      self.zona_id = zona_id
    end

    def get
      map_locations
    end

    private
      def parser
        @parser ||= Nokogiri::HTML(body)
      end

      def body
        get_tre.body
      end

      def collect_locations
        @collected_locations = []
        parser_conteudo.each_slice(4){|locations| @collected_locations << locations }
        return @collected_locations
      end

      def map_locations
        @keys = collect_locations.shift
        return collect_locations.collect{|ar| Location.new(@keys.zip(ar).to_h) }
      end

      def get_tre
        @get_tre ||= HTTPI.get(request)
      end

      def request
        @request = HTTPI::Request.new
        @request.url = @@default_url
        @request.query = {acao: 'zona', zona: zona_id}
        @request
      end

      def parser_conteudo
        parser.css('div#conteudo table tr td').collect(&:text)
      end

  end
end