class ImportBulletinUrn
  attr_accessor :file_name, :values, :delete_headers, :state

  def initialize(file_name, layout, state)
    extend layout.constantize
    self.file_name = file_name
    self.state = state
    self.delete_headers = ["candidate_code", "candidate_name",
        "electoral_section_zone", "electoral_section_section",
        "electoral_section_polling_place", "political_parties_code",
        "political_parties_name","political_office_code", "political_office_name",
        "amout_elector","amount_voter_not_appeared","amount_voter_appeared"]
  end

  def import
    bar = ProgressBar.new(number_lines, :bar, :rate, :eta, :percentage, :elapsed)
    file = CSV.open(file_name.to_s, col_sep: ";")
    @cache = []
    file.each do |row|
      self.values = Hash[[self.header, row].transpose]
      self.values.merge!({candidate: candidate,
        electoral_section: electoral_section, political_party: political_party, political_office: political_office
      })
      election
      self.values.reject!{|k,v| self.header_delete.include?(k) || delete_headers.include?(k) }
      bu = BulletinUrnsCandidatesElectoralSectionsPoliticalParties.find_or_create_by(values)
      bu.save
      bar.increment!
    end
  end

  private
    # def bulletin_urn
    #   @bulletin_urn ||= find_or_create_bulletin_urn
    # end

    def election
      find_in_cache_election || find_or_create_election
    end

    def number_lines
      `wc -l #{file_name}`.to_i
    end

    def candidate
      find_in_cache_candidate || find_or_create_candidate
    end

    def electoral_section
      find_in_cache_electoral_section || find_or_create_electoral_section
    end

    def political_party
      find_in_cache_political_party || find_or_create_political_party
    end

    def political_office
      find_in_cache_political_office || find_or_create_political_office
    end

    # def find_or_create_bulletin_urn
    #   BulletinUrn.find_or_create_by(code: values["bulletin_urn_code"])
    # end

    def find_or_create_candidate
      @candidates = nil
      Candidate.find_or_create_by(code: values["candidate_code"],name: values["candidate_name"])
    end

    def find_or_create_electoral_section
      @electoral_sections = ElectoralSection.find_or_initialize_by(zone: values["electoral_section_zone"],
        section: values["electoral_section_section"], state: self.state )
      if @electoral_sections.new_record? then
        @electoral_sections.polling_place = values["electoral_section_polling_place"]
        @electoral_sections.save
      end
      @electoral_sections
    end

    def find_or_create_political_party
      @political_parties = nil
      PoliticalParty.find_or_create_by(code: values["political_parties_code"],name: values["political_parties_name"])
    end

    def find_or_create_political_office
      @political_offices = nil
      PoliticalOffice.find_or_create_by(code: values["political_office_code"],name: values["political_office_name"])
    end

    def candidates
      @candidates ||= Candidate.all
    end

    def electoral_sections
      @electoral_sections
    end

    def political_parties
      @political_parties ||= PoliticalParty.all
    end

    def political_offices
      @political_offices ||= PoliticalOffice.all
    end

    def elections
      @elections
    end

    def find_in_cache_candidate
      candidates.select{|cand| cand.name == values["candidate_namse"] &&
      cand.code == values["candidate_code"]}.first
    end

    def find_in_cache_electoral_section
      return nil if electoral_sections.nil?
      ( (electoral_sections.zone == values["electoral_section_zone"] &&
        electoral_sections.section == values["electoral_section_section"] &&
        electoral_sections.state == self.state) ?
          electoral_sections : nil
       )
    end

    def find_in_cache_political_party
      political_parties.select{|political_part| political_part.code == values["political_parties_code"] &&
         political_part.name == values["political_parties_name"]}.first
    end

    def find_in_cache_political_office
      political_offices.select{|political_offic| political_offic.code == values["political_office_code"] &&
        political_offic.name == values["political_office_name"]  }.first
    end

    def find_in_cache_election
      return nil if elections.nil?
      (elections.election_number == values["election_number"] &&
        elections.electoral_section.id == self.values[:electoral_section].id ) ? elections : nil
    end

    def find_or_create_election
      @elections = ElectionsElectoralSection.find_or_initialize_by(
        election_number: values["election_number"],
        electoral_section_id: self.values[:electoral_section].id)
      if @elections.new_record? then
        @elections.number_of_voters = values["amout_elector"]
        @elections.number_of_voters_not_appeared = values["amount_voter_not_appeared"]
        @elections.number_of_voters_appeared = values["amount_voter_appeared"]
        raise @elections.inspect unless @elections.save
      end
      @elections
    end

end