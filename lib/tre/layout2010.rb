# -*- encoding : utf-8 -*-
module Tre
 module Layout2010
  @@header = ["generate_date","generate_hour","political_office_code",
  "political_office_name","electoral_section_zone","electoral_section_section","NÚMERO DO LOCAL",
  "political_parties_code","political_parties_name","CÓDIGO DA LOCALIDADE",
  "electoral_section_polling_place","bulletin_urn_date_received_at","amout_elector",
  "amount_voter_not_appeared","amount_voter_appeared","TIPO DE ELEIÇÃO",
  "CÓDIGO DO TIPO DE ORIGEM DO VOTO","urn_type",
  "DESCRIÇÃO DO TIPO DE URNA",
  "candidate_code","candidate_name","vote_amount",
  "CÓDIGO DE TIPO DO VOTÁVEL",
  "NÚMERO URNA EFETIVADA","CÓDIGO DE CARGA DE URNA1 EFETIVADA",
  "CÓDIGO DE CARGA DE URNA2 EFETIVADA",
  "DATA DE CARGA DA URNA EFETIVADA","CÓDIGO DE FLASH CARD EFETIVADA",
  "CARGO SEÇÃO",
  "election_number" ]

  @@header_delete = ["generate_date","generate_hour","NÚMERO DO LOCAL",
  "NÚMERO DO PARTIDO","SIGLA DO PARTIDO","CÓDIGO DA LOCALIDADE",
  "TIPO DE ELEIÇÃO","CÓDIGO DO TIPO DE ORIGEM DO VOTO","DESCRIÇÃO DO TIPO DE URNA",
  "CÓDIGO DE TIPO DO VOTÁVEL","NÚMERO URNA EFETIVADA","CÓDIGO DE CARGA DE URNA1 EFETIVADA",
  "CÓDIGO DE CARGA DE URNA2 EFETIVADA","DATA DE CARGA DA URNA EFETIVADA","CÓDIGO DE FLASH CARD EFETIVADA",
  "CARGO SEÇÃO","bulletin_urn_date_received_at","urn_type"]

  def header
    @@header
  end

  def header_delete
    @@header_delete
  end

  end
end
