class ImportVoteLocation
  attr_accessor :file_name
  @@header = ["state", "section", "zone", "COD_MUNIC_TSE", "NM_MUNIC_TSE",
       "COD_MUNIC_IBGE", "city", "capital",
       "COD_MUNIC_ECT", "COD_BAIRRO_TSE", "district",
       "COD_BAIRRO_ECT", "COD_LOCALVOTACAO_TSE",
       "street", "zip_code", "polling_place",
       "active"]

  @@header_delete = ["COD_MUNIC_TSE", "NM_MUNIC_TSE",
       "COD_MUNIC_IBGE", "COD_MUNIC_ECT", "COD_BAIRRO_TSE",
       "COD_BAIRRO_ECT", "COD_LOCALVOTACAO_TSE" ]

  def initialize(file_name)
    self.file_name = file_name
  end

  def import
    bar = ProgressBar.new(10000, :bar, :rate, :eta, :percentage, :elapsed)
    file = CSV.open(file_name.to_s, col_sep: ";")
    @cache = []
    file.each do |row|
      hash = Hash[[@@header, row].transpose]
      hash.reject!{|k,v| @@header_delete.include?(k) }
      location_urna = ElectoralSection.new(hash)
      location_urna.geocode
      location_urna.save
      bar.increment!
    end
  end
end