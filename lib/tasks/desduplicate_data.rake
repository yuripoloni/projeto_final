
namespace :desduplucate do
  desc "Desduplicate date electoralsection"
  task :electoral_section => :environment do
    size = ElectoralSection.where(city: nil, state: "RS", zone: ["16","169"] ).count
    bar = ProgressBar.new(size, :bar, :rate, :eta, :percentage, :elapsed)
    ElectoralSection.where(city: nil, state: "RS", zone: ["16","169"] ).collect{|a|
      duplicate = ElectoralSection.where(state: a.state, zone: a.zone, section: a.section).where("city is not null")
      next if !duplicate.one?
      duplicate = duplicate.first
      next if duplicate.city.nil?
      bar.increment!
      BulletinUrnsCandidatesElectoralSectionsPoliticalParties.where(electoral_section_id: a.id
        ).update_all(electoral_section_id: duplicate.id)
    }
  end
end
