require 'tre'

namespace :import do
  namespace :vote do
    desc "Import location machine vote"
    task :location2012 => :environment do
      skip = ['.','..']
      path = Rails.root.join('private','files','location2012').to_s
      Dir.foreach(path) do |file|
        p "import file: #{file}"
        if skip.include?(file) then
          p "skipped"
          next
        end
        ImportSectionWorker.perform_async(path + '/' + file)
      end
    end

    desc "Geocode all location 2012 without coordinates"
    task :geocode => :environment do
      size = ElectoralSection.count()
      bar = ProgressBar.new(size, :bar, :rate, :eta, :percentage, :elapsed)
      ElectoralSection.not_geocoded.find_in_batches(batch_size: 1000) do |electoral_sections|
        ActiveRecord::Base.transaction do
          electoral_sections.each{|electoral_section|
            electoral_section.save
            bar.increment!
            sleep(0.25)
          }
        end
      end
    end

  end

  namespace :bulletin_urn do

    desc "Import bulletin urn"
    task :layout2010 => :environment do
      skip = ['.','..']
      path = Rails.root.join('private','files','bu2010').to_s
      Dir.foreach(path) do |file|
        p "import file: #{file}"
        if skip.include?(file) then
          p "skipped"
          next
        end
        ImportBulletinUrnWorker.perform_async(path + '/' + file, "Tre::Layout2010", file.split("_")[0])
      end
    end

  end
end