// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require chartkick
//= require select2
//= require select2_locale_pt-BR
//= require_tree .

function debug(msg) {
  if (window.console) {
    console.info(msg);
  }
}

$(function() {
   // init geral do sistema
  Core.actual = {};
  Core.actual.controller = $('body').attr('data-controller');
  Core.actual.action = $('body').attr('data-action');

  try { Core[Core.actual.controller][Core.actual.action]['init'].call();
  } catch(e) {
    console.debug(e);
    console.debug(e.stack);
    console.debug("\"Core." + Core.actual.controller + "." + Core.actual.action + ".init()\" não existe.");
  }
});