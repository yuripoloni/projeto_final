if (window.Core === undefined) window.Core = {};

Core.district_audits = {

  index: function() {

    var priv = {};
    var pub = {};

    pub.init = function(){
      $('#audit_state').bind('change', priv.find_state);
    }

    priv.find_state = function(){
      if( $(this).val() ) {
        $("#loading").show();
        $.getJSON('/cities', { state: $(this).val(), format: "json"}).done(priv.update_select_city);
      }
    }

    priv.update_select_city = function( data ) {
      var options = $.map( data, function( i, item ) {
        return $("<option>").val(i).html(i);
      });

      $("#audit_city").html(options);
    }

    return pub;
  }()
}

