json.array!(@boletim_urnas) do |boletim_urna|
  json.extract! boletim_urna, :id
  json.url boletim_urna_url(boletim_urna, format: :json)
end
