class BoletimUrnasController < ApplicationController
  before_action :set_boletim_urna, only: [:show]

  # GET /boletim_urnas
  # GET /boletim_urnas.json
  def index
    #ids = [25, 26, 34, 73, 94, 98, 128, 147, 173, 210, 268, 364, 374, 377, 380, 397]
    @boletim_urnas = BoletimUrna.summarize_vote_of_candidate_in_the_box#.where(numero_secao: ids)
  end

  # GET /boletim_urnas/1
  # GET /boletim_urnas/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_boletim_urna
      @boletim_urna = BoletimUrna.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def boletim_urna_params
      params[:boletim_urna]
    end
end
