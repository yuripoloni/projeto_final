class DataMiningController < ApplicationController

  # GET /data_mining
  # GET /data_mining.json
  def index
    @q = BulletinUrnsCandidatesElectoralSectionsPoliticalParties.search(params)
    @data_mining = DataMining.new(params)
  end

end
