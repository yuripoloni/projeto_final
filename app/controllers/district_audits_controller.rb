class DistrictAuditsController < ApplicationController
  before_action :load_resources, only: [:show, :show_in_table]
  # GET /district_audits
  # GET /district_audits.json
  def index
    @states = ElectoralSection.group(:state).order(:state).pluck(:state)
    @process = redis.keys("audit_*").collect{|k| JSON::parse(redis.get(k)).merge(id: k.split('_').last) }
  end

  def create
    @ubs = ElectoralSection.where({state: audit_params[:state], city: audit_params[:city]}).group("district").pluck(:district)
    redis_key = 'audit_'+ Digest::MD5.hexdigest(audit_params.to_json)
    if redis.get(redis_key).blank? && @ubs.any?
      redis.set(redis_key, { params: audit_params, total: @ubs.count}.to_json)
      @ubs.each{|ub|
         AuditWorker.perform_async({q: {electoral_section_state_eq: audit_params[:state], electoral_section_city_eq: audit_params[:city],
            electoral_section_district_eq: ub}}, redis_key: redis_key)
      }
    end

    redirect_to action: :index
  end

  def show

  end

  def show_in_table
    @candidates = @results.first["candidates"].sort{|a,b| a["candidate_name"]<=>b["candidate_name"] }
  end

  private
    def audit_params
      params.require(:audit).permit(:state, :city)
    end

    def redis
      @redis ||= Redis.new
    end

    def load_resources
      id = params[:id]||params[:district_audit_id]
      @audit = JSON::parse(redis.get('audit_'+id))
      @results = redis.keys(id+'*').collect{|k| JSON::parse(redis.get(k)) }
    end


end