class BulletinUrnsCandidatesElectoralSectionsPoliticalPartiesController < ApplicationController

  def index
    @bus = BulletinUrnsCandidatesElectoralSectionsPoliticalParties.all.order(:electoral_section_id,:candidate_id).limit(50)
  end

  def edit
    @bu = BulletinUrnsCandidatesElectoralSectionsPoliticalParties.find(params[:id])
  end

  def update
    @bu = BulletinUrnsCandidatesElectoralSectionsPoliticalParties.find(params[:id])
    if @bu.update_attributes(bu_params) then
      redirect_to action: :index
    else
      render :edit
    end
  end

  private
    def bu_params
      params.require(:bulletin_urns_candidates_electoral_sections_political_parties).permit(:vote_amount)
    end



end