class CitiesController <  ApplicationController
  respond_to :json

  def index
    @cities = ElectoralSection.where(state: params[:state]).group(:city).order(:city).pluck(:city)
    render json: @cities.compact.to_json
  end

end