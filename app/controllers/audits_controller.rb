class AuditsController < ApplicationController

  # GET /audits
  # GET /audits.json
  def index
    @q = BulletinUrnsCandidatesElectoralSectionsPoliticalParties.search(params)
    @city = ElectoralSection.group(:city,:state).order(:state)
    @zones = ElectoralSection.group(:zone).pluck(:zone).sort{|a,b| a.to_i <=> b.to_i }
    @candidates = PoliticalOffice.find_by_name("Presidente").candidates.distinct
    @audits = DataMining.new(params: params[:q],epsilon: params[:epsilon].try(:to_i)||2,
    min_points: params[:min_points].try(:to_i)||1) unless params[:q].blank?
    @states = ElectoralSection.group(:state).order(:state).pluck(:state)
  end

end