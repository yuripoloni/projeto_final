class ImportBulletinUrnWorker
  include Sidekiq::Worker

  def perform(file_path, layout, state)
    bulletin_urn = ImportBulletinUrn.new(file_path, layout, state)
    bulletin_urn.import
  end
end