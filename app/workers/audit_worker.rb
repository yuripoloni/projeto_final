class AuditWorker
  include Sidekiq::Worker

  def perform(params, redis_key)
    Audit.new(params.merge(redis_key)).results
  end
end