class ImportSectionWorker
  include Sidekiq::Worker

  def perform(file_path)
    vote_location = ImportVoteLocation.new(file_path)
    vote_location.import
  end
end