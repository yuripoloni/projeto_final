class PoliticalOffice < ActiveRecord::Base
  has_many :bulletin_urns_candidates_electoral_sections_political_parties
  has_many :candidates, through: :bulletin_urns_candidates_electoral_sections_political_parties

end