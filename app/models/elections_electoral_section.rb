class ElectionsElectoralSection < ActiveRecord::Base
  belongs_to :electoral_section
  validates_uniqueness_of :election_number, scope: [:electoral_section_id]

end
