class BulletinUrnsCandidatesElectoralSectionsPoliticalParties < ActiveRecord::Base
  belongs_to :bulletin_urn
  belongs_to :candidate
  belongs_to :electoral_section
  belongs_to :political_party
  belongs_to :political_office

  validates_uniqueness_of :election_number, scope: [:candidate_id, :electoral_section_id,
    :political_office_id, :political_party_id]

  scope :calculate_epsilon, ->(electoral_section_ids,candidate_id) {
    self.unscoped.select("( MAX((vote_amount*100)/elections_electoral_sections.number_of_voters_appeared) - MIN((vote_amount*100)/elections_electoral_sections.number_of_voters_appeared)) /2 as epsilon").joins("LEFT JOIN electoral_sections ON bulletin_urns_candidates_electoral_sections_political_parties.electoral_section_id = electoral_sections.id LEFT JOIN elections_electoral_sections ON electoral_sections.id = elections_electoral_sections.electoral_section_id LEFT JOIN candidates ON bulletin_urns_candidates_electoral_sections_political_parties.candidate_id = candidates.id").where(electoral_section_id: electoral_section_ids, candidate_id: candidate_id)
  }
end

class BulletinUrnsCandidatesElectoralSectionsPoliticalParty < BulletinUrnsCandidatesElectoralSectionsPoliticalParties
end



