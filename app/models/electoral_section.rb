# -*- encoding : utf-8 -*-
class ElectoralSection < ActiveRecord::Base
  geocoded_by :full_address   # can also be an IP address
  after_validation :geocode          # auto-fetch coordinates
  has_one :elections_electoral_section

  def full_address
    [street, city, state, 'BRAZIL'].compact.join(', ')
  end

end