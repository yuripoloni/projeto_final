class DataMining < OpenStruct
  # DataMining.new(params: { :electoral_section_state_eq=>"RS", :electoral_section_zone_eq=>"169", :candidate_id_eq=>169})
  def initialize(params)
    @first = 0
    @second = 0
    @aux = 0
    super params
  end

  def dbscan
    @dbscan ||= DBSCAN( values, :epsilon => epsilon, :min_points => min_points, :distance => :euclidean_distance, labels: labels )
  end

  private
    def values
      self.labels = []
      bulletin_urns_candidates.collect{|candidate|
        # [candidate.electoral_section.latitude, candidate.electoral_section.longitude,
         self.labels << candidate.electoral_section.id
         [percent(candidate.electoral_section.elections_electoral_section.try(:number_of_voters), candidate.vote_amount, @aux)]
      }
    end

    def bulletin_urns_candidates
      @bulletin_urns_candidates ||= BulletinUrnsCandidatesElectoralSectionsPoliticalParties.includes(:candidate,
        :electoral_section, { electoral_section: :elections_electoral_section}).search(params).result
    end

    def percent(total,value,id)
      # [10,13,16,19,9,14,17,20].sample
      # [10,13,16,19,26,27,31,33].sample
      # [26,27,31,30,26,27,30,31,].sample

      return 0 if total.nil? || value.nil?
      (value*100)/total
      # @aux += 1
      # if @aux > 10 && @aux < 20 then
      #   @aux.even? ? 9 : 8
      # else
      #   @aux.even? ? 20 : 21
      # end
      # return 46 if @aux == 1
      # return 1 if @aux == 2
      # if @aux > 10 && @aux < 20 then
      #   @aux.even? ? 50 : 49
      #   # @second += 1
      #   # (56 - @second)
      # elsif @aux > 20 then
      #   @first += 1
      #   (43 - @first)
      # else
      #   @aux.even? ? 60 : 59
      #   # 60
      # end
    end

end