class Audit < OpenStruct

  def results
    @results = candidates.collect{|candidate|
      @epsilon = nil
      res = mining(candidate.id).dbscan
      res.clusters.reject!{|k,v| v.blank? }
      if res.clusters.one? && res.clusters.has_key?(-1) then
        {status: :undetermined, candidate_id:  candidate.id, candidate_name: candidate.name }
      else
        {candidate_id:  candidate.id, candidate_name: candidate.name,
         groups: res.clusters.keys.collect{|k| { k => generate_group(res, k) } }
        }
      end
    }.flatten
    redis.set(redis_district_key, to_json )
  end

  private
    def mining(candidate_id)
      conditions = q.except("electoral_section_district_eq","electoral_section_state_eq","electoral_section_city_eq").merge(candidate_id_eq: candidate_id, electoral_section_id_in: find_electoral_sections.pluck(:id))
      DataMining.new(params: conditions, epsilon: 6,
        min_points: 1) unless q.blank?
    end

    def candidates
      @candidates ||= PoliticalOffice.find_by_name("Presidente").candidates.distinct
    end

    def redis
      @redis ||= Redis.new
    end

    def redis_district_key
      redis_key.split('_').last + '_' + Digest::MD5.hexdigest(q["electoral_section_district_eq"])
    end

    def find_electoral_section(id)
      ElectoralSection.find(id)
    end

    def find_electoral_sections
      ElectoralSection.where(district: q["electoral_section_district_eq"],
        state: q["electoral_section_state_eq"], city: q["electoral_section_city_eq"])
    end

    def to_json
      { district: q["electoral_section_district_eq"],
        candidates: @results
      }.to_json
    end

    def generate_group(res, k)
      group = res.clusters[k].collect{|v|
        electoral_section = find_electoral_section(v.label)
         { section: electoral_section.section, zone: electoral_section.zone, calculated: v.items.first }
      }.flatten
    end
end
